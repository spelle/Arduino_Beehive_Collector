/*
 * Beehive_Collector_EEPROM.h
 *
 *  Created on: Aug 28, 2015
 *      Author: sherpa
 */

#ifndef ARDUINO_BEEHIVE_COLLECTOR_EEPROM_H_
#define ARDUINO_BEEHIVE_COLLECTOR_EEPROM_H_


#include <Arduino_Beehive_Unit_EEPROM.h>


typedef struct
{
	beehiveId_t	beehiveList[256/sizeof(beehiveId_t)] ; // <==> 64, providing beehiveId_t is 4 byte long
} beehiveList_EEPROM_t ;


#endif /* ARDUINO_BEEHIVE_COLLECTOR_EEPROM_H_ */
