/*
 * Arduino_Beehive_Scale.cpp
 *
 *  Created on: Oct 3, 2014
 *      Author: spelle
 */
#include "Arduino_Beehive_Collector.h"



#include <SoftwareSerial.h>
#include <EEPROM.h>

#ifdef ETHERNET
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#endif

#include <avr/wdt.h>
#include <util/crc16.h>


#include "Arduino_Beehive_Scale_Data.h"

#include "Arduino_Beehive_Unit_EEPROM.h"
#include "Arduino_Beehive_Collector_EEPROM.h"

#include "beehiveFieldProtocol.h"
#include <utility/util.h>



#define READ_BEEHIVES_PERIOD	 5000
#define READ_BEEHIVE_PERIOD      1000

#define BLINK_LED_PERIOD         1000



typedef enum
{
	initState,

	dataFrameBuild,
	dataFrameSend,
	dataFrameSent,
	dataFrameReceive,
	dataFrameComplete,
	dataFrameValid,

#ifdef ETHERNET
	serverLogOnFrameBuild,
	serverLogOnFrameSend,
	serverLogOnFrameSent,
	serverLogOnFrameReceive,
	serverLogOnFrameComplete,
	serverLogOnFrameValid,

	serverBeehiveDataFrameBuild,
	serverBeehiveDataFrameSend,
	serverBeehiveDataFrameSent,
	serverBeehiveDataFrameReceive,
	serverBeehiveDataFrameComplete,
	serverBeehiveDataFrameValid,

	serverLogOffFrameBuild,
	serverLogOffFrameSend,
	serverLogOffFrameSent,
	serverLogOffFrameReceive,
	serverLogOffFrameComplete,
	serverLogOffFrameValid,
#endif

	endState,
} processBeehiveStateMachine_t ;



//#ifndef ARDUINO_AVR_MEGA
SoftwareSerial SWSerial(6, 7) ; // RX, TX
//#endif
#define TX_ENABLE_PIN 9



#define unitd_EEPROM_ADDRESS 0
unitId_EEPROM_t unitId_EEPROM =
{
		0x87654321
} ;

#define beehiveList_EEPROM_ADDRESS 256
beehiveList_EEPROM_t beehiveList_EEPROM =
{
	0x12345678,
	0x00000000
};


byte macAddress[] =
{
		0x90, 0xA2, 0xDA, 0x0F, 0xCE, 0x6F
} ;

#ifdef ETHERNET
const unsigned int  udpLocalPort                      = 8888 ;				// local port to listen for UDP packets
const          char udpRemoteServerAddress[]          = "192.168.1.50" ;	// remote server address to connect to

               byte udpRequestBuffer[MAX3(sizeof(serverLogOnRequest_t),sizeof(beehiveDataFrameRequest_t),sizeof(serverLogOffRequest_t))];		//buffer to hold outgoing packets
               byte udpResponseBuffer[MAX3(sizeof(serverLogOnResponse_t),sizeof(beehiveDataFrameResponse_t),sizeof(serverLogOffResponse_t))];	//buffer to hold incoming packets

EthernetUDP Udp ; // A UDP instance to let us send and receive packets over UDP
#endif

void setup()
{
	// Set PB5 as OUTPUT
	DDRB |= (1 << PB5 ) ;

	// Initialize SWSerial Link
	SWSerial.begin( FIELD_LINK_BAUD_RATE ) ;
	pinMode( TX_ENABLE_PIN, OUTPUT ) ;
	digitalWrite( TX_ENABLE_PIN, LOW ) ;

	// Read unitId from EEPROM
	unitId_EEPROM_t unitId ;
	EEPROM.get( unitd_EEPROM_ADDRESS, unitId) ;
	if( unitId.unitId == 0x00 )
		EEPROM.put( unitd_EEPROM_ADDRESS, unitId_EEPROM ) ;

	EEPROM.get( unitd_EEPROM_ADDRESS, unitId_EEPROM) ;

	// Read beehiveList from EEPROM
	// unitId_t beehiveId = 0x00000000 ;
	// EEPROM.get( beehiveList_EEPROM_ADDRESS, beehiveId) ;
	// for( uint8_t i = 0 ; beehiveId != 0x00000000 ; EEPROM.get( beehiveList_EEPROM_ADDRESS + i*sizeof(unitId_t), beehiveId) , i++ )
	// {
	// 	beehiveList_EEPROM.beehiveList[i] = beehiveId ;
	// }

	EEPROM.put( beehiveList_EEPROM_ADDRESS, beehiveList_EEPROM ) ;

	// for( uint8_t i = 0 ; beehiveList_EEPROM.beehiveList[i] != 0x00000000 ; EEPROM.put( beehiveList_EEPROM_ADDRESS + i*sizeof(unitId_t), beehiveList_EEPROM.beehiveList[i]) , i++ ) ;

	// for( uint8_t i = 0 ; beehiveList_EEPROM.beehiveList[i] != 0x00000000 ; EEPROM.get( beehiveList_EEPROM_ADDRESS + i*sizeof(unitId_t), beehiveList_EEPROM.beehiveList[i]) , i++ ) ;

	DEBUG_INIT_SERIAL ;

	PDEBUG( "--- Arduino Beehive Collector ---\n" ) ;
	PDEBUG( "- unit ID : 0x%08lX\n", unitId_EEPROM.unitId ) ;
	PDEBUG( "- beehiveList :\n" ) ;
	for( uint8_t i = 0 ; beehiveList_EEPROM.beehiveList[i] != 0x00000000 ; i++ )
		PDEBUG( "\t- [%i] : 0x%08lX\n", i, beehiveList_EEPROM.beehiveList[i] ) ;

#ifdef ETHERNET
	// start Ethernet and UDP
	if (Ethernet.begin(macAddress) == 0) {
		PDEBUG("Failed to configure Ethernet using DHCP");
		// no point in carrying on, so do nothing forevermore:
		for (;;)
			;
	}

	Udp.begin( udpLocalPort ) ;
#endif
}

void loop()
{
	static unsigned long ulCurrentTime          = 0 ;
	static unsigned long ulNextTimeReadBeehives = READ_BEEHIVES_PERIOD ;
	static unsigned long ulNextTimeReadBeehive  = READ_BEEHIVE_PERIOD ;

	static unsigned long ulNextTimeLED          = BLINK_LED_PERIOD ;

	static processBeehiveStateMachine_t	processBeehiveStateMachine = initState ;

	static boolean						processReadBeehives = false ;
	static boolean						processReadBeehive  = false ;

	static beehiveReadDataRequest_t		beehiveReadDataRequest ;
	static uint8_t						beehiveListCounter = 0 ;
	static byte *						pBeehiveRequest = (byte *) &beehiveReadDataRequest ;
	static uint8_t						uiSendBeehiveDataReadRequestCptr = 0 ;

	static uint8_t						nbBeehiveDataReadRequestRetry = 0 ;

	static beehiveReadDataResponse_t	beehiveReadDataResponse ;
	static byte *						pBeehiveResponse = (byte *) &beehiveReadDataResponse ;
	static uint8_t						uiRcvdBeehiveDataReadResponseCptr = 0 ;

	uint16_t CRC = 0x0000 ;
#ifdef ETHERNET
	static serverLogOnRequest_t			*pServerLogOnRequest        = (serverLogOnRequest_t *)      &udpRequestBuffer[0] ;
	static beehiveDataFrameRequest_t	*pBeehiveDataFrameRequest   = (beehiveDataFrameRequest_t *) &udpRequestBuffer[0] ;
	static serverLogOffRequest_t		*pServerLogOffRequest       = (serverLogOffRequest_t *)     &udpRequestBuffer[0] ;
//	static uint8_t			beehiveListCounter = 0 ;
//	static byte *			pBeehiveRequest = (byte *) &beehiveReadDataRequest ;
//	static uint8_t			uiSendBeehiveDataReadRequestCptr = 0 ;
//
	//static uint8_t						nbServerRequestRetry = 0 ;

	static serverLogOnResponse_t		*pServerLogOnResponse        = (serverLogOnResponse_t *)      &udpResponseBuffer[0] ;
	static beehiveDataFrameResponse_t	*pBeehiveDataFrameResponse   = (beehiveDataFrameResponse_t *) &udpResponseBuffer[0] ;
	static serverLogOffResponse_t		*pServerLogOffResponse       = (serverLogOffResponse_t *)     &udpResponseBuffer[0] ;
//	static byte *			pBeehiveResponse = (byte *) &beehiveReadDataResponse ;
//	static uint8_t			uiRcvdBeehiveDataReadResponseCptr = 0 ;

	static sessionKey_t					sessionKey = 0x00000000 ;
#endif

	DEBUG_ROTATE ;

	ulCurrentTime = millis() ;

	// launch Read Beehives Requests
	if( ulCurrentTime > ulNextTimeReadBeehives )
	{
		beehiveListCounter = 0 ;
		processReadBeehives = true ;

		ulNextTimeReadBeehives = ulCurrentTime + READ_BEEHIVES_PERIOD ;
	}

	// process a Read Beehive Request
	if( (true == processReadBeehives) && (ulCurrentTime > ulNextTimeReadBeehive ) )
	{
		if( initState == processBeehiveStateMachine )
			processReadBeehive = true ;

		if( endState == processBeehiveStateMachine )
		{
			PDEBUG( "previous request ended with success after %hhi attempts. Processes next beehive in list.\n", nbBeehiveDataReadRequestRetry ) ;
			nbBeehiveDataReadRequestRetry = 0 ;

			// Process next beehive
			beehiveListCounter ++ ;

			// Relaunch state machine
			processReadBeehive = true ;
			processBeehiveStateMachine = initState ;
		}

		// After NB_MAX attempt...
		if( NB_MAX_RETRY <= nbBeehiveDataReadRequestRetry )
		{
			PDEBUG( "NB_MAX_RETRY reached.\n" ) ;
			nbBeehiveDataReadRequestRetry = 0 ;

			// Process next beehive
			beehiveListCounter ++ ;

			// Relaunch state machine
			processReadBeehive = true ;
			processBeehiveStateMachine = initState ;
		}

		if	( dataFrameReceive == processBeehiveStateMachine )
		{	// Still in responseReceive state after READ_BEEHIVE_PERIOD seconds ; timeout conditions
			PDEBUG( "Serial Timeout.\n" ) ;
			nbBeehiveDataReadRequestRetry ++ ;

			// Relaunch state machine
			processReadBeehive = true ;
			processBeehiveStateMachine = initState ;
		}

		if	(
				(serverLogOnFrameReceive       == processBeehiveStateMachine) ||
				(serverBeehiveDataFrameReceive == processBeehiveStateMachine) ||
				(serverLogOffFrameReceive      == processBeehiveStateMachine)
			)
		{	// Still in responseReceive state after READ_BEEHIVE_PERIOD seconds ; timeout conditions
			PDEBUG( "UDP Timeout.\n" ) ;

			// Relaunch state machine
			processBeehiveStateMachine = initState ;
		}
		else if
			(
				serverLogOnFrameSent == processBeehiveStateMachine
			)
		{
			processBeehiveStateMachine = serverLogOnFrameReceive ;
		}
		else if
			(
				serverBeehiveDataFrameSent == processBeehiveStateMachine
			)
		{
			processBeehiveStateMachine = serverBeehiveDataFrameReceive ;
		}
		else if
			(
				serverLogOffFrameSent == processBeehiveStateMachine
			)
		{
			processBeehiveStateMachine = serverLogOffFrameReceive ;
		}

		// At end end of list ?
		if ( (0x00000000 == beehiveList_EEPROM.beehiveList[beehiveListCounter]) )
		{
			// Reset state machine
			PDEBUG( "End of list. Stop processing.\n" ) ;

			processReadBeehive = false ;
			processReadBeehives = false ;
			processBeehiveStateMachine = initState ;
		}

		ulNextTimeReadBeehive = ulCurrentTime + READ_BEEHIVE_PERIOD ;
	}

	switch( processBeehiveStateMachine )
	{
		case initState :

			// process a Read Beehive Request
			if( true == processReadBeehive )
			{
				//PDEBUG( "- processBeehiveStateMachine : initState\n" ) ;
				//PDEBUG( "\t- Triggered\n" ) ;

				processReadBeehive = false ;

				memset( (void *) &beehiveReadDataRequest, 0, sizeof(beehiveReadDataRequest) ) ;
				memset( (void *) &beehiveReadDataResponse, 0, sizeof(beehiveReadDataResponse) ) ;

				digitalWrite( TX_ENABLE_PIN, LOW ) ;

				uiSendBeehiveDataReadRequestCptr = 0 ;
				uiRcvdBeehiveDataReadResponseCptr = 0 ;

				processBeehiveStateMachine = dataFrameBuild ;
			}

			break ;

		case dataFrameBuild :

			//PDEBUG( "processBeehiveStateMachine : dataFrameBuild\n" ) ;

			// Build the Request
			beehiveReadDataRequest.frameSize = sizeof(beehiveReadDataRequest) ;
			beehiveReadDataRequest.serviceId = beehiveDataReadRequest ;
			beehiveReadDataRequest.beehiveId = htonl(beehiveList_EEPROM.beehiveList[beehiveListCounter]) ;

			// Calculate the CRC
			CRC = 0xffff ;

			for( uint8_t i = 0 ; i < (beehiveReadDataRequest.frameSize - sizeof( beehiveReadDataRequest.CRC)) ; i ++ )
				CRC = _crc_ccitt_update( CRC, pBeehiveRequest[i] ) ;

			beehiveReadDataRequest.CRC       = htons(CRC) ;

			TRACE( (const char *)&beehiveReadDataRequest, beehiveReadDataRequest.frameSize ) ;

			// Send it
			uiSendBeehiveDataReadRequestCptr = 0 ;
			digitalWrite( TX_ENABLE_PIN, HIGH ) ;
			processBeehiveStateMachine = dataFrameSend ;

			break ;

		case dataFrameSend :

			SWSerial.write( pBeehiveRequest[uiSendBeehiveDataReadRequestCptr] ) ;

			// //PDEBUG( "%02X\n", (uint8_t) pBeehiveRequest[uiSendBeehiveDataReadRequestCptr] ) ;
			uiSendBeehiveDataReadRequestCptr ++ ;

			if( sizeof(beehiveReadDataRequest) <= uiSendBeehiveDataReadRequestCptr )
			{	// At end of "buffer"
				processBeehiveStateMachine = dataFrameSent ;
				//PDEBUG( "processBeehiveStateMachine : dataFrameSend\n" ) ;
			}

			break ;

		case dataFrameSent :

			//PDEBUG( "processBeehiveStateMachine : dataFrameSent\n" ) ;

			digitalWrite( TX_ENABLE_PIN, LOW ) ;

			//PDEBUG( "\nprocessBeehiveStateMachine : dataFrameReceive\n" ) ;

			uiRcvdBeehiveDataReadResponseCptr = 0 ;

			processBeehiveStateMachine = dataFrameReceive ;

			break;

		case dataFrameReceive :

			// Read the SWSeerial
			if ( SWSerial.available() )
			{
				pBeehiveResponse[uiRcvdBeehiveDataReadResponseCptr] = SWSerial.read() ;

				uiRcvdBeehiveDataReadResponseCptr ++ ;

				if( sizeof(beehiveReadDataResponse) <= uiRcvdBeehiveDataReadResponseCptr )
				{	// we may have a complete message...
					processBeehiveStateMachine = dataFrameComplete ;
				}
			}

			break ;

		case dataFrameComplete :

			//PDEBUG( "processBeehiveStateMachine : responseComplete\n" ) ;
			TRACE( (const char *)&beehiveReadDataResponse, beehiveReadDataResponse.frameSize ) ;

			// Calculate the CRC
			CRC = 0xffff ;

			for( uint8_t i = 0 ; i < (beehiveReadDataResponse.frameSize - sizeof( beehiveReadDataResponse.CRC)) ; i ++ )
				CRC = _crc_ccitt_update( CRC, pBeehiveResponse[i] ) ;

			if(
				( beehiveReadDataResponse.serviceId == beehiveDataReadResponse ) &&
				( beehiveReadDataResponse.beehiveId == htonl(beehiveList_EEPROM.beehiveList[beehiveListCounter]) ) &&
				( beehiveReadDataResponse.CRC       == htons(CRC) )
			)
				processBeehiveStateMachine = dataFrameValid ;
			else
				processBeehiveStateMachine = endState ;

			break ;

		case dataFrameValid :

			//PDEBUG( "processBeehiveStateMachine : responseValid\n" ) ;

			//PDEBUG( "%lu : Beehive Scale ID : Ox%08lX\n", ulCurrentTime, htonl(beehiveReadDataResponse.beehiveId) ) ;

			//PDEBUG( "%lu : Beehive Gauge #1 : %hu\n", ulCurrentTime, htons(beehiveReadDataResponse.gaugeNo1Value) ) ;

			//PDEBUG( "%lu : Beehive Scale Raw Value : %hu\n", ulCurrentTime, htons(beehiveReadDataResponse.scaleRawValue) ) ;

			processReadBeehive = false ;

#ifndef ETHERNET
			processBeehiveStateMachine = endState ;
#else
			processBeehiveStateMachine = serverLogOnFrameBuild ;
#endif
			break ;

#ifdef ETHERNET
		case serverLogOnFrameBuild :

			//PDEBUG( "processBeehiveStateMachine : serverLogOnFrameBuild\n" ) ;

			Udp.flush() ;

			memset( (void *) &beehiveReadDataRequest, 0, sizeof(beehiveReadDataRequest) ) ;
			memset( (void *) &beehiveReadDataResponse, 0, sizeof(beehiveReadDataResponse) ) ;

			// Build the Request
			pServerLogOnRequest->frameSize = sizeof(serverLogOnRequest_t) ;
			pServerLogOnRequest->serviceId = logOn ;
			pServerLogOnRequest->collectorId = htonl(unitId_EEPROM.unitId) ;
			pServerLogOnRequest->protocolVersion= htons(BEEHIVE_PROTOCOL_VERSION) ;
			pServerLogOnRequest->CRC = htons(0xA55A) ;

			TRACE( (const char *)&udpRequestBuffer[0], udpRequestBuffer[0] ) ;

			// Send it
			processBeehiveStateMachine = serverLogOnFrameSend ;

			break ;

		case serverLogOnFrameSend :

			Udp.beginPacket( udpRemoteServerAddress, BEEHIVE_PROTOCOL_UDP_PORT ) ;
			Udp.write( (unsigned char *)&udpRequestBuffer[0] , udpRequestBuffer[0] ) ;
			Udp.endPacket() ;

			processBeehiveStateMachine = serverLogOnFrameSent ;

			// no break ;

		case serverLogOnFrameSent :

			//PDEBUG( "processBeehiveStateMachine : serverLogOnFrameSent\n" ) ;

			memset( udpResponseBuffer, 0x00, sizeof udpResponseBuffer ) ;
			Udp.flush() ;

			//processBeehiveStateMachine = serverLogOnFrameReceive ;

			break;

		case serverLogOnFrameReceive :

			if ( Udp.parsePacket() )
			{
				// We've received a packet, read the data from it
				Udp.read( (unsigned char *)&udpResponseBuffer[0] , sizeof(udpResponseBuffer) ) ; // read the packet into the buffer
				TRACE( (const char *)&udpResponseBuffer[0], udpResponseBuffer[0] ) ;

				processBeehiveStateMachine = serverLogOnFrameComplete ;
			}

			break ;

		case serverLogOnFrameComplete :

			// PDEBUG( "processBeehiveStateMachine : serverLogOnFrameComplete\n" ) ;

			if	(
					( pServerLogOnResponse->frameSize  == sizeof(serverLogOnResponse_t) ) &&
					( pServerLogOnResponse->serviceId  == logOn_Ack ) &&
					( pServerLogOnResponse->sessionKey == htonl(0x12345678) ) &&
					( pServerLogOnResponse->errorCode  == success ) &&
					( pServerLogOnResponse->CRC        == htons(0x5AA5) )
				)
				processBeehiveStateMachine = serverLogOnFrameValid ;
			else
				processBeehiveStateMachine = endState ;

			break ;

		case serverLogOnFrameValid :

			//PDEBUG( "processBeehiveStateMachine : serverLogOnFrameValid\n" ) ;

			sessionKey = ntohl(pServerLogOnResponse->sessionKey) ;

			processBeehiveStateMachine = serverBeehiveDataFrameBuild ;

			break ;

		case serverBeehiveDataFrameBuild :

			//PDEBUG( "processBeehiveStateMachine : serverBeehiveDataFrameBuild\n" ) ;

			memset( udpRequestBuffer, 0x00, sizeof udpRequestBuffer ) ;

			// Build the Request
			pBeehiveDataFrameRequest->frameSize = sizeof(beehiveDataFrameRequest_t) ;
			pBeehiveDataFrameRequest->serviceId = beehiveDataFrame ;

			pBeehiveDataFrameRequest->sessionKey = htonl(sessionKey) ;

			pBeehiveDataFrameRequest->beehiveId			= htonl(beehiveList_EEPROM.beehiveList[beehiveListCounter]) ;

			pBeehiveDataFrameRequest->gaugeNo1Value		= htons(beehiveReadDataResponse.gaugeNo1Value)    ;
			pBeehiveDataFrameRequest->gaugeNo1RawValue	= htons(beehiveReadDataResponse.gaugeNo1RawValue) ;

			pBeehiveDataFrameRequest->gaugeNo2Value		= htons(beehiveReadDataResponse.gaugeNo2Value)    ;
			pBeehiveDataFrameRequest->gaugeNo2RawValue	= htons(beehiveReadDataResponse.gaugeNo2RawValue) ;

			pBeehiveDataFrameRequest->gaugeNo3Value		= htons(beehiveReadDataResponse.gaugeNo3Value)    ;
			pBeehiveDataFrameRequest->gaugeNo3RawValue	= htons(beehiveReadDataResponse.gaugeNo3RawValue) ;

			pBeehiveDataFrameRequest->scaleValue		= htons(beehiveReadDataResponse.scaleValue)       ;
			pBeehiveDataFrameRequest->scaleRawValue		= htons(beehiveReadDataResponse.scaleRawValue)    ;

			pBeehiveDataFrameRequest->CRC = htons(0xA55A) ;

			TRACE( (const char *)&udpRequestBuffer[0], udpRequestBuffer[0] ) ;

			processBeehiveStateMachine = serverBeehiveDataFrameSend ;

			break ;

		case serverBeehiveDataFrameSend :

			Udp.beginPacket( udpRemoteServerAddress, BEEHIVE_PROTOCOL_UDP_PORT ) ;
			Udp.write( (unsigned char *)&udpRequestBuffer[0] , udpRequestBuffer[0] ) ;
			Udp.endPacket() ;

			processBeehiveStateMachine = serverBeehiveDataFrameSent ;

			break ;

		case serverBeehiveDataFrameSent :

			//PDEBUG( "processBeehiveStateMachine : serverBeehiveDataFrameSent\n" ) ;

			memset( udpResponseBuffer, 0x00, sizeof udpResponseBuffer ) ;
			Udp.flush() ;

			//processBeehiveStateMachine = serverBeehiveDataFrameReceive ;

			break;

		case serverBeehiveDataFrameReceive :

			if ( Udp.parsePacket() )
			{
				// We've received a packet, read the data from it
				Udp.read( (unsigned char *)&udpResponseBuffer[0] , sizeof(udpResponseBuffer) ) ; // read the packet into the buffer

				TRACE( (const char *)&udpResponseBuffer[0], udpResponseBuffer[0] ) ;
			}

			processBeehiveStateMachine = serverBeehiveDataFrameComplete ;

			break ;

		case serverBeehiveDataFrameComplete :

			//PDEBUG( "processBeehiveStateMachine : serverBeehiveDataFrameComplete\n" ) ;

			if	(
					( pBeehiveDataFrameResponse->frameSize  == sizeof(beehiveDataFrameResponse_t) ) &&
					( pBeehiveDataFrameResponse->serviceId  == beehiveDataFrame_Ack ) &&
					( pBeehiveDataFrameResponse->sessionKey == htonl(0x12345678) ) &&
					( pBeehiveDataFrameResponse->errorCode  == success ) &&
					( pBeehiveDataFrameResponse->CRC        == htons(0x5AA5) )
				)
				processBeehiveStateMachine = serverBeehiveDataFrameValid ;
			else
				processBeehiveStateMachine = endState ;

			break ;

		case serverBeehiveDataFrameValid :

			//PDEBUG( "processBeehiveStateMachine : serverBeehiveDataFrameValid\n" ) ;

			sessionKey = ntohl(pServerLogOnResponse->sessionKey) ; // htonl ?

			processBeehiveStateMachine = serverLogOffFrameBuild ;

			break ;

		case serverLogOffFrameBuild :

			PDEBUG( "processBeehiveStateMachine : serverLogOffFrameBuild\n" ) ;

			memset( udpRequestBuffer, 0x00, sizeof udpRequestBuffer ) ;

			// Build the Request
			pServerLogOffRequest->frameSize  = sizeof(serverLogOffRequest_t) ;
			pServerLogOffRequest->serviceId  = logOff ;

			pServerLogOffRequest->sessionKey = htonl(sessionKey) ;

			pServerLogOffRequest->CRC        = htons(0xA55A) ;

			TRACE( (const char *)&udpRequestBuffer[0], udpRequestBuffer[0] ) ;

			processBeehiveStateMachine = serverLogOffFrameSend ;

			break ;

		case serverLogOffFrameSend :

			//PDEBUG( "processBeehiveStateMachine : serverLogOffFrameSend\n" ) ;

			Udp.beginPacket( udpRemoteServerAddress, BEEHIVE_PROTOCOL_UDP_PORT ) ;
			Udp.write( (unsigned char *)&udpRequestBuffer[0] , udpRequestBuffer[0] ) ;
			Udp.endPacket() ;

			processBeehiveStateMachine = serverLogOffFrameSent ;

			break ;

		case serverLogOffFrameSent :

			//PDEBUG( "processBeehiveStateMachine : serverLogOffFrameSent\n" ) ;

			memset( udpResponseBuffer, 0x00, sizeof udpResponseBuffer ) ;
			Udp.flush() ;

			//processBeehiveStateMachine = serverLogOffFrameReceive ;

			break ;

		case serverLogOffFrameReceive :

			//PDEBUG( "processBeehiveStateMachine : serverLogOffFrameReceive\n" ) ;

			if ( Udp.parsePacket() )
			{
				// We've received a packet, read the data from it
				Udp.read( (unsigned char *)&udpResponseBuffer[0] , sizeof(udpResponseBuffer) ) ; // read the packet into the buffer

				TRACE( (const char *)&udpResponseBuffer[0], udpResponseBuffer[0] ) ;
			}

			processBeehiveStateMachine = serverLogOffFrameComplete ;

			break ;

		case serverLogOffFrameComplete :

			//PDEBUG( "processBeehiveStateMachine : serverLogOffFrameComplete\n" ) ;

			if	(
					( pServerLogOffResponse->frameSize  == sizeof(serverLogOffResponse_t) ) &&
					( pServerLogOffResponse->serviceId  == logOff_Ack ) &&
					( pServerLogOffResponse->sessionKey == htonl(0x12345678) ) &&
					( pServerLogOffResponse->errorCode  == success ) &&
					( pServerLogOffResponse->CRC        == htons(0x5AA5) )
				)
				processBeehiveStateMachine = serverLogOffFrameValid ;
			else
				processBeehiveStateMachine = endState ;

			break ;

		case serverLogOffFrameValid :

			PDEBUG( "processBeehiveStateMachine : serverLogOffFrameValid\n" ) ;

			// Nothing to do

			processBeehiveStateMachine = endState ;

			break ;


#endif

		case endState :

			break ;
	}

	// Watchdog LED
	if( ulCurrentTime > ulNextTimeLED )
	{
		PORTB ^= (1 << PB5 ) ;

		ulNextTimeLED = ulCurrentTime + BLINK_LED_PERIOD ;
	}
}
